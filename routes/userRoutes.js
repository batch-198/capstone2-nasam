const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

// register
router.post('/register',userControllers.registerUser);

// login
router.post('/login',userControllers.loginUser);

// set user admin
router.put('/addAdmin/:userId',verify,verifyAdmin,userControllers.addAdminUser);

// get user details
router.get('/userDetails',verify,userControllers.getUserDetails);

module.exports = router;