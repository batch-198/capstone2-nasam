const express = require("express");
const router = express.Router();

// models
const Product = require("../models/Product");

// controller
const productControllers = require("../controllers/productControllers");

// Verify & Verify Admin
const auth = require("../auth");
const {verify, verifyAdmin} = auth;


// Without Route Params


// Create Product (admin only)
router.post('/addProduct',verify,verifyAdmin,productControllers.createProduct);

// Get all Active Products
router.get('/getActiveProducts',productControllers.activeProducts);

// Get All Products
router.get('/all',productControllers.allProducts); 



// with Route Params


// Retrieve Single Product
router.get('/:productId',productControllers.retrieveSingleProduct);

// Update Product Info (admin only)
router.put('/updateProduct/:productId',verify,verifyAdmin,productControllers.updateProduct);

// Archive Product (admin only)
router.delete('/archiveProduct/:productId',verify,verifyAdmin,productControllers.archiveProduct);

// Activate Product (admin only)
router.put('/activateProduct/:productId',verify,verifyAdmin,productControllers.activateProduct);


// Search by Product Name
router.post('/searchProduct',productControllers.searchProduct);


module.exports = router;