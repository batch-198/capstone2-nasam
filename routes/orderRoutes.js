const express = require("express");
const router = express.Router();

const orderControllers = require("../controllers/orderControllers");

const auth = require("../auth");
const {verify, verifyAdmin} = auth;


// Create Order
router.post('/addOrder',verify,orderControllers.addOrder);

// Retrieve All Order (admin only)
router.get('/allOrders',verify,verifyAdmin,orderControllers.getAllOrders);

// Retrieve Authenticated User's Order
router.get('/myOrders',verify,orderControllers.getMyOrder);

// Get Order Details
router.get('/myOrderDetails/:orderId',verify,orderControllers.getMyOrderDetails);

module.exports = router;