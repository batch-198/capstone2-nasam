const jwt = require("jsonwebtoken");

const secret = "reveMythicStoreAPI"


// Login
module.exports.createAccessToken = (userDetails) => {

	console.log(userDetails);
	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}

	return jwt.sign(data,secret,{});
}

// Verify all Users
module.exports.verify = (req,res,next) => {

	let token = req.headers.authorization

	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token."})
	} else {
		token = token.slice(7);

		jwt.verify(token,secret,function(err,decodedToken){
			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else {
				req.user = decodedToken;
				next();
			}
		})
	}
}


// For Admin Only Functions
module.exports.verifyAdmin = (req,res,next) => {

	if(req.user.isAdmin){
		next();
	} else {

		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}