const Product = require("../models/Product");

// Create Product
module.exports.createProduct = (req,res) => {

	let newProduct = new Product ({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
	})

	newProduct.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


// Retrieve Single Product
module.exports.retrieveSingleProduct = (req,res) => {
	Product.findById(req.params.productId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// Get all active products
module.exports.activeProducts = (req,res) => {
	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// Get all Products
module.exports.allProducts = (req,res) => {
	Product.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

// Update Product Info (admin only)
module.exports.updateProduct = (req,res) => {
	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// Archive Product (admin only)
module.exports.archiveProduct = (req,res) => {
	Product.findByIdAndUpdate(req.params.productId,{"isActive": "false"},{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// Activate Product (admin only)
module.exports.activateProduct = (req,res) => {
	Product.findByIdAndUpdate(req.params.productId,{"isActive": "true"},{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// Search by Product Name
module.exports.searchProduct = (req,res) => {
   Product.find({"name": {$regex: req.body.name, $options: 'i'}})
   .then(result => res.send(result))
   .catch(error => res.send(error))
}