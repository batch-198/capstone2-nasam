const Order = require("../models/Order");


// Create Order
module.exports.addOrder = async (req,res) => {
	// console.log(req.body);
	// console.log(req.user);

/*
	CHECKOUT
	1. Check IF user is not admin
	3. CREATE Order
	4. SAVE the order
*/

	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden"});
	} else {	
		let newOrder = new Order ({
			totalAmount: req.body.totalAmount,
			userId: req.user.id,
			products: req.body.products
		})

		return newOrder.save()
		.then(result => res.send({message: "Order Successful"}))
		.catch(error => res.send(error))
	}
}

// Get all orders (admin only)
module.exports.getAllOrders = (req,res) => {
	Order.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


// Get all orders (User)
module.exports.getMyOrder = (req,res) => {
	Order.find({userId: req.user.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// Get Order Details
module.exports.getMyOrderDetails = (req,res) => {
	Order.findOne({id: req.params.orderId}, {products:1})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}