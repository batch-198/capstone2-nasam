//user model
const User = require("../models/User");
// bcrypt
const bcrypt = require("bcrypt");
// auth
const auth = require("../auth");


// User Registration
module.exports.registerUser = (req,res) => {
	const hashedPw = bcrypt.hashSync(req.body.password,10);

	let newUser = new User ({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo
	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// User Login/Authentication
module.exports.loginUser = (req,res) => {
	User.findOne({email:req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send({message: "No User Found. Please register"})
		} else {
			const passwordCheck = bcrypt.compareSync(req.body.password,foundUser.password)

			if(passwordCheck){
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send({message: "Please check your credentials"});
			}
		}
	})
}

// Set User as Admin
module.exports.addAdminUser = (req,res) => {
	User.findByIdAndUpdate(req.params.userId,{"isAdmin":"true"},{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// Get User Details
module.exports.getUserDetails = (req,res) => {
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}