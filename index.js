// express
const express = require("express");
const app = express();

// mongoose
const mongoose = require("mongoose");

const port = 4000;

// mongoose connection
mongoose.connect("mongodb+srv://admin:admin123@cluster0.bewdg.mongodb.net/reveStoreAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

let db = mongoose.connection;
db.on('error', console.error.bind(console, "MongoDB Connection Error"));
db.once('open',()=>console.log("Connected to MongoDB"))

app.use(express.json());

// User Route
const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

// Product Route
const productRoutes = require('./routes/productRoutes');
app.use('/products',productRoutes);

// Order Route
const orderRoutes = require('./routes/orderRoutes');
app.use('/orders',orderRoutes);

app.listen(port,() => 
	console.log(`Server is running at port 4000`));