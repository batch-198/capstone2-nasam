const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is Required"]
		},
	lastName: {
		type: String,
		required: [true, "Last Name is Required"]
		},
	email:	{
		type: String,
		required: [true, "E-mail is Required"]
		},
	password: {
		type: String,
		required: [true, "Password is Required"]
		},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is Required"]
		},
	isAdmin: {
		type: Boolean,
		default: false
		}
})

module.exports = mongoose.model("User",userSchema);