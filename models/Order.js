const mongoose = require("mongoose"); 

const orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String,
		required: [true, "userId is required"]
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "productId is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
		}
	]
})

module.exports = mongoose.model("Order",orderSchema);